//
//  SKBoard.swift
//  SlideStack
//
//  Created by Alex Mai on 2/7/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKBoard: SKSpriteNode {
    var data: [[Piece]]
    //goes from 1 to amtofplayers. Player 0 is nothing.
    var turnIndictor: Int
    let amtOfPlayers: Int
    var gravDirect = Directs.Up
    let spacing: CGFloat
    let pieceW: CGFloat
    let fallSpeed = NSTimeInterval(0.2)
    
    init(width: Int, height: Int, amountOfPlayers: Int, size: CGSize) {
        spacing = size.width / CGFloat(width + 1) / CGFloat(width + 1)
        pieceW = size.width / CGFloat(width + 1)
        let placeholder = Piece(value: 0, size: CGSize(
            width: size.width / CGFloat(width + 1),
            height: size.width / CGFloat(width + 1))
        )
        placeholder.hidden = true
        data = [[Piece]](count: width, repeatedValue: [Piece](count: height, repeatedValue: placeholder))
        amtOfPlayers = amountOfPlayers
        turnIndictor = 1
        super.init(texture: nil, color: beige, size: size)
        
        self.removeAllChildren()
        
        let placementGuide = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: self.size)
        placementGuide.anchorPoint = CGPointZero
        placementGuide.position = CGPoint(x: -self.size.width / 2, y: -self.size.height / 2)
        self.addChild(placementGuide)
        
        for x in 0..<data.count {
            for y in 0..<data[0].count {
                let ph = Piece(value: 0, size: CGSize(
                    width: size.width / CGFloat(width + 1),
                    height: size.width / CGFloat(width + 1))
                )
                ph.hidden = true
                ph.position = posFor(x, y: y)
                data[x][y] = ph
                placementGuide.addChild(data[x][y])
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var numArray: [[Int]] {
        get {
            var a = [[Int]](count: width, repeatedValue: [Int](count: height, repeatedValue: 0))
            for x in 0..<width {
                for y in 0..<height {
                    a[x][y] = data[x][y].value
                }
            }
            return a
        }
    }
    
    var width: Int {
        get {
            return data.count
        }
    }
    
    var height: Int {
        get {
            return data[0].count
        }
    }
    
    func skip() {
        if (turnIndictor == amtOfPlayers) {
            turnIndictor = 1
        } else {
            turnIndictor++
        }
    }
    
    var isDone: Bool {
        get {
            for x in data {
                for i in x {
                    if (i == 0) {return false}
                }
            }
            return true
        }
    }
    
    //Returns how far down it went
    func place(i: Int) -> Int {
        //Starts at the bottom and goes up to scan for empty slots
        switch gravDirect {
        case .Up:
            let index = i
            // If the top row isn't full
            if data[index][data[index].count - 1] == 0 {
                var distanceDown = data[0].count + 1
                for i in 0..<data[index].count {
                    distanceDown--
                    if data[index][i] == 0 {
                        data[index][i].position = posFor(index, y: height)
                        data[index][i].hidden = false
                        data[index][i].runAction(SKAction.moveTo(posFor(index, y: i), duration: 0.5))
                        data[index][i].value = turnIndictor
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        case .Down:
            let index = data.count - i - 1
            // If the bottom row isn't full
            if data[index][0] == 0 {
                var distanceDown = data[0].count + 1
                for y in 0..<data[index].count {
                    distanceDown--
                    let i = data[index].count - y - 1
                    if data[index][i] == 0 {
                        data[index][i].position = posFor(index, y: 0)
                        data[index][i].hidden = false
                        data[index][i].runAction(SKAction.moveTo(posFor(index, y: i), duration: 0.5))
                        data[index][i].value = turnIndictor
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        case .Left:
            let index = i
            if data[0][index] == 0 {
                //Y is fixed to the selected row, go through each value for x and look at the fixed y height
                var distanceDown = data.count + 1
                for y in 0..<data.count {
                    distanceDown--
                    let i = data.count - y - 1
                    //If it is free, turn it and stop
                    if data[i][index] == 0 {
                        data[i][index].position = posFor(0, y: index)
                        data[i][index].value = turnIndictor
                        data[i][index].hidden = false
                        data[i][index].runAction(SKAction.moveTo(posFor(i, y: index), duration: 0.5))
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        case .Right:
            let index = data[0].count - i - 1
            if data[data.count - 1][index] == 0 {
                //Y is fixed to the selected row, go through each value for x and look at the fixed y height
                var distanceDown = data.count + 1
                for i in 0..<data.count {
                    distanceDown--
                    //If it is free, turn it and stop
                    if data[i][index] == 0 {
                        data[i][index].position = posFor(data.count - 1, y: index)
                        data[i][index].value = turnIndictor
                        data[i][index].hidden = false
                        data[i][index].runAction(SKAction.moveTo(posFor(i, y: index), duration: 0.5))
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    
    //Returns (x, y, amount moved in new direction)
    func shift(direction: Directs) {
        //Store lowest piece per row
        self.gravDirect = direction
        
        //Start from the bottom and start shift items down
        switch gravDirect {
        case .Up:
            for x in 0..<data.count {
                var furthestEmptySlot = 0
                for y in 0..<data[0].count {
                    //If the current slot has a piece
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == y) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot++
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            data[x][y].hidden = true
                            data[x][furthestEmptySlot].value = data[x][y].value
                            data[x][furthestEmptySlot].position = posFor(x, y: y)
                            data[x][furthestEmptySlot].hidden = false
                            data[x][furthestEmptySlot].runAction(SKAction.moveTo(posFor(x, y: furthestEmptySlot), duration: fallSpeed))
                            data[x][y].value = 0
                            furthestEmptySlot++
                        }
                    }
                }
            }
        case .Down:
            for x in 0..<data.count {
                var furthestEmptySlot = data[0].count - 1
                //Count backwards
                for p in 0..<data[0].count {
                    let y = data[0].count - p - 1
                    //If the current slot has a piece
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == y) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot--
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            data[x][y].hidden = true
                            data[x][furthestEmptySlot].value = data[x][y].value
                            data[x][furthestEmptySlot].position = posFor(x, y: y)
                            data[x][furthestEmptySlot].hidden = false
                            data[x][furthestEmptySlot].runAction(SKAction.moveTo(posFor(x, y: furthestEmptySlot), duration: fallSpeed))
                            data[x][y].value = 0
                            furthestEmptySlot--
                        }
                    }
                }
            }
        case .Left:
            for y in 0..<data[0].count {
                var furthestEmptySlot = data.count - 1
                for p in 0..<data.count {
                    let x = data.count - p - 1
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == x) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot--
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            data[x][y].hidden = true
                            data[furthestEmptySlot][y].value = data[x][y].value
                            data[furthestEmptySlot][y].position = posFor(x, y: y)
                            data[furthestEmptySlot][y].hidden = false
                            data[furthestEmptySlot][y].runAction(SKAction.moveTo(posFor(furthestEmptySlot, y: y), duration: fallSpeed))
                            data[x][y].value = 0
                            furthestEmptySlot--
                        }
                    }
                }
            }
        case .Right:
            for y in 0..<data[0].count {
                var furthestEmptySlot = 0
                for x in 0..<data.count {
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == x) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot++
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            data[x][y].hidden = true
                            data[furthestEmptySlot][y].value = data[x][y].value
                            data[furthestEmptySlot][y].position = posFor(x, y: y)
                            data[furthestEmptySlot][y].hidden = false
                            data[furthestEmptySlot][y].runAction(SKAction.moveTo(posFor(furthestEmptySlot, y: y), duration: fallSpeed))
                            data[x][y].value = 0
                            furthestEmptySlot++
                        }
                    }
                }
            }
        default:
            NSException(name: "NAD", reason: "A non-acceptable direction was sent to Board.swift", userInfo: nil).raise()
        }
    }
    
    //Int player who won
    func scanForConnect(length: Int) -> [Int: Bool]? {
        var winners : [Int: Bool] = [:]
        var didEnd = false
        for i in 1...amtOfPlayers {
            
            //Add the player to the list
            winners[i] = false
            
            //Scan for straight up
            for x in 0..<data.count {
                var currentChain = 0
                
                for y in 0..<data[0].count {
                    if data[x][y] == i {
                        currentChain++
                        if currentChain >= length {
                            winners[i] = true
                            didEnd = true
                        }
                    } else {
                        currentChain = 0
                    }
                }
            }
            
            //Scan for horizontal
            for y in 0..<data[0].count {
                var currentChain = 0
                
                for x in 0..<data.count {
                    if data[x][y] == i {
                        currentChain++
                        if currentChain >= length {
                            winners[i] = true
                            didEnd = true
                        }
                    } else {
                        currentChain = 0
                    }
                }
            }
            
            //For the amount of diagonal need to be cycled through {
            ////add the difference between the width and height to the smaller value
            ////iterate thourgh the width, when width is smaller than the i, iterate through the height
            //}
            //Make a point at the top
            //Subtract 1 from x and y - loop until one of the values reaches 0

            //Cover the asterisks
            for p in 0..<width {
                //Always start at the one of the points at the top
                var x = p
                var y = height - 1
                var isDone = false
                var count = 0
                while (!isDone) {
                    if data[x][y].value == i {
                        count++
                    } else {
                        count = 0
                    }
                    if count >= length {
			winners[i] = true
			didEnd = true
                    }

                    //Increment
                    ////We are going from top to bottom and from left to right
                    x++
                    y--
                    
                    //Make sure it doesn't overstep
                    isDone = !(x < width && y >= 0)
                }
            }
            
            //Cover the dashes
            for p in 0..<height-1 {
                //Always start at the one of the points at the top
                var x = 0
                var y = p
                var isDone = false
                var count = 0
                while (!isDone) {
                    if data[x][y].value == i {
                        count++
                    } else {
                        count = 0
                    }
					
                    if count >= length {
						winners[i] = true
						didEnd = true
					}
					
                    //Increment
                    ////We are going from top to bottom and from left to right
                    x++
                    y--
                    
                    //Make sure it doesn't overstep
					//As long as these two conditions are not false, its not done
                    isDone = !(x < width && y >= 0)
                }
            }

            //Cover the asterisks
            for p in 0..<width {
                //Always start at the one of the points at the top
                var x = p
                var y = height - 1
                var isDone = false
                var count = 0
                while (!isDone) {
                    if data[x][y].value == i {
                        count++
                    } else {
                        count = 0
                    }
                    if count >= length {
			winners[i] = true
			didEnd = true
                    }

                    //Increment
                    ////We are going from the top right to the bottom left
                    x--
                    y--
                    
                    //Make sure it doesn't overstep
                    isDone = !(x >= 0 && y >= 0)
                }
            }
            
            //Cover the dashes
            for p in 0..<height-1 {
                //Always start at the one of the points at the top
                var x = width-1
                var y = p
                var isDone = false
                var count = 0
                while (!isDone) {
                    if data[x][y].value == i {
                        count++
                    } else {
                        count = 0
                    }
					
                    if count >= length {
						winners[i] = true
						didEnd = true
					}
					
                    //Increment
                    ////We are going from the top right to the bottom left
                    x--
                    y--
                    
                    //Make sure it doesn't overstep
					//As long as these two conditions are not false, its not done
                    isDone = !(x >= 0 && y >= 0)
                }
            }

            /*
            //Scan for diagonal up left to down right
            for iy in 0..<data[0].count {
                var y = data[0].count - iy - 1
                var currentChain = 0
                for ny in y..<data[0].count {
                    var nx = data.count - (ny - y) - 1
                    var iny = data[0].count - ny - 1
                    println("x: \(nx), y: \(iny)")
                    if data[nx][iny] == i {
                        currentChain++
                        if currentChain >= length {
                            winners[i] = true
                            didEnd = true
                        }
                    } else {
                        currentChain = 0
                    }
                }
            }
            
            //Top corner
            for iy in 0..<data[0].count {
                var y = data[0].count - iy - 1
                var currentChain = 0
                for ny in y..<data[0].count {
                    var x = (ny - y)
                    if data[x][ny] == i {
                        currentChain++
                        if currentChain >= length {
                            winners[i] = true
                            didEnd = true
                        }
                    } else {
                        currentChain = 0
                    }
                }
            }
            
            //Scan for diagonal down left to up right
            
            for iy in 0..<data[0].count {
                var y = data[0].count - iy - 1
                var currentChain = 0
                for x in 0...y {
                    var ny = y - x
                    if data[x][ny] == i {
                        currentChain++
                        if currentChain >= length {
                            winners[i] = true
                            didEnd = true
                        }
                    } else {
                        currentChain = 0
                    }
                }
            }
            
            //Top Corner
            for iy in 0..<data[0].count {
                var y = data[0].count - iy - 1
                var currentChain = 0
                for ny in y..<data[0].count {
                    var x = data.count - (ny - y) - 1
                    if data[x][ny] == i {
                        currentChain++
                        if currentChain >= length {
                            winners[i] = true
                            didEnd = true
                        }
                    } else {
                        currentChain = 0
                    }
                }
            }*/
            
        }
        //if there are winners, return winners, otherwise return nil
        if didEnd {
            return winners
        } else {
            return nil
        }
    }
    
    func clear() {
        for x in 0..<data.count {
            for y in 0..<data[0].count {
                data[x][y].value = 0
                data[x][y].hidden = true
            }
        }
    }
    
    func posFor(x: Int, y: Int) -> CGPoint {
        let spacesX = CGFloat(x + 1) * spacing
        let x = (((CGFloat(x)) * pieceW) + (pieceW / 2)) + spacesX
        
        let spacesY = CGFloat(y + 1) * spacing
        let y = (((CGFloat(y)) * pieceW) + (pieceW / 2)) + spacesY
        
        return CGPoint(x: x, y: y)
    }
}
