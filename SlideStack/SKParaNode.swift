//
//  SKParaNode.swift
//  FlipSide
//
//  Created by Alex Mai on 3/12/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKParaNode: SKNode {
	var text: String {
		didSet {
			refresh()
		}
	}
	var fontName: String = "HelveticaNeue-UltraLight" {
		didSet {
			for i in nodes {
				i.fontName = fontName
			}
		}
	}
	var fontSize: CGFloat = 20 {
		didSet {
			for i in nodes {
				i.fontSize = fontSize
			}
		}
	}
	var alignment: Directs = .None {
		didSet {
			switch(alignment) {
			case .Left:
				for i in nodes {
					i.position.x = 0
					i.horizontalAlignmentMode = .Left
				}
			case .Right:
				for i in nodes {
					i.position.x = 0
					i.horizontalAlignmentMode = .Right
				}
			case .None:
				for i in nodes {
					i.position.x = 0
					i.horizontalAlignmentMode = .Center
				}
			default:
				()
			}
		}
	}
	var charWidth: Int = 20 
	{
		didSet {
			refresh()
		}
	}
	var spacing: CGFloat = 20
	{
		didSet {
			for i in 0..<nodes.count {
            	nodes[i].position = CGPoint(x: 0, y:(CGFloat(i) * -spacing))
			}
		}
	}
    var color: UIColor = UIColor.blackColor() {
		didSet {
			for i in nodes {
				i.changeColor(color)
			}
		}
	}
    var breakLines: Bool = false {
        didSet {
            refresh()
        }
    }
	var nodes: [SKLabelNode] = []

	init(text: String) {
		self.text = text
		super.init()
		refresh()
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

    func sliceByLength(var text: String, sliceLengths: Int, breakWords: Bool) -> [String] {
        var slices: [String] = []

		if breakWords {
			let numOfLines = Int(ceil( Double(text.characters.count) / Double(sliceLengths) ))
			for _ in 0..<numOfLines {
				//Pop off a segment of the string
				//Copy string
				var segment = text
                if segment.characters.count < sliceLengths {
                    segment = segment.substringToIndex(text.startIndex.advancedBy(segment.characters.count))
                } else {
                    segment = segment.substringToIndex(text.startIndex.advancedBy(sliceLengths))
                    text = text.substringFromIndex(text.startIndex.advancedBy(sliceLengths))
                }
				slices.append(segment)
			}
		} else {
			let words = text.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
			
			var lNum = 0

			for i in 0..<words.count {
				if slices.count >= lNum {
					slices.append("")
				}
				slices[lNum] += (words[i] + " ")
                
                if i == words.count-1 {
                    break
                }

				if (slices[lNum] + words[i+1]).characters.count > sliceLengths {
					lNum++
				}
			}
		}
        return slices
    }

	func nodesForStrings(strings: [String]) -> [SKLabelNode] {
		var labelnodes: [SKLabelNode] = []

		for i in strings {
			let node = SKLabelNode(text: i)
			node.fontSize = fontSize
			node.fontName = fontName
            switch(alignment) {
            case .Left:
                node.horizontalAlignmentMode = .Left
            case .Right:
                node.horizontalAlignmentMode = .Right
            case .None:
                node.horizontalAlignmentMode = .Center
            default:
                ()
            }
			labelnodes.append(node)
		}

		return labelnodes
	}

	func refresh() {
		self.removeAllChildren()
		nodes = nodesForStrings(sliceByLength(text, sliceLengths: charWidth, breakWords: breakLines))
		for i in 0..<nodes.count {
            nodes[i].position = CGPoint(x: 0, y:(CGFloat(i) * -spacing))
			self.addChild(nodes[i])
		}
        
	}
}
