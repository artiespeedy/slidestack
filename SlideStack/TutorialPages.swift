//
//  TutorialPages.swift
//  FlipSide
//
//  Created by Alex Mai on 3/12/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

let moveLeft = SKAction.moveByX(-screenSize.width, y: 0, duration: 0.4)
let moveRight = SKAction.moveByX(screenSize.width, y: 0, duration: 0.4)

func wwPara(text: String) -> SKParaNode {
    let h = screenSize.height
    let w = screenSize.width
    let p1 = SKParaNode(text: text)
    p1.fontSize = 30
    p1.spacing = 30
    p1.position.y = h - (h * 4)/10
    p1.position.x = w/20
    p1.alignment = .Left
    p1.changeColor(beige)
    p1.charWidth = Int(w * 2.4) / 33
    return p1
}

func ex(name: String) -> SKSpriteNode {
    let mx = screenSize.width / 2
    let h = screenSize.height
    
    let e1 = SKSpriteNode(imageNamed: name)
    e1.size = CGSize(width: ((mx * 3.7) / 2), height: ((mx * 3.7) / 2))
    e1.anchorPoint = CGPointZero
    e1.position = CGPoint(x: 0, y: (h * 2) / 10)
    
    return e1
}

func mainTutPage() -> SKNode {
    let mx = screenSize.width / 2
    let h = screenSize.height
    let w = screenSize.width
    
    let n = SKNode()
    
    //Items
    
    let p1 = wwPara("FlipSide is a twist on Connect Four.")
    p1.fontSize = 40
    p1.charWidth = Int(w * 2.4) / 40
    p1.spacing = 35
    
    let p2 = wwPara("The goal is still to align four pieces, but instead of dropping a piece players can use their turn to turn the board and flip gravity.")
    
    let n1 = SKNode()
    
    let b1 = SKTextButton(text: "Strategy", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.3) / 2), height: h / 15), functionToCall: {})
    b1.functionToCall = {
        ()->() in
        b1.functionToCall = {}
        let next = stratsTutPage()
        next.position.x = screenSize.width
        next.runAction(moveLeft)
        n.parent?.addChild(next)
        n.runAction(moveLeft, completion: {
            n.removeFromParent()
        })
    }
    b1.position = CGPoint(x: ((mx * 0.2) / 2), y: (h * 3)/20)
    
    let b2 = SKTextButton(text: "Connect 4?", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.3) / 2), height: h / 15), functionToCall: {})
    b2.functionToCall = {
        ()->() in
        b2.functionToCall = {}
        let next = refreshTutPage()
        next.position.x = screenSize.width
        next.runAction(moveLeft)
        n.parent?.addChild(next)
        n.runAction(moveLeft, completion: {
            n.removeFromParent()
        })
    }
    b2.position = CGPoint(x: ((mx * 0.2) / 2), y: (h * 5)/20)
    
    let b3 = SKTextButton(text: "Settings", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.3) / 2), height: h / 15), functionToCall: {})
    b3.functionToCall = {
        ()->() in
        b3.functionToCall = {}
        let next = settingsTutPage()
        next.position.x = screenSize.width
        next.runAction(moveLeft)
        n.parent?.addChild(next)
        n.runAction(moveLeft, completion: {
            n.removeFromParent()
        })
    }
    b3.position = CGPoint(x: ((mx * 0.2) / 2), y: (h * 7)/20)
    
    let p3 = wwPara("To flip gravity, swipe the board in the new direction of gravity.")
    
    n1.addChild(b1)
    n1.addChild(b2)
    n1.addChild(b3)
    n1.addChild(p3)
    
    let rotator = SKRotator(width: ((mx * 3.7) / 2), startIndex: 0, elements: p1, p2, n1)
    rotator.position.x = mx - ((mx * 3.7) / 4)
    rotator.position.y = h - (h * 8)/10
    rotator.aniSpeed = 0.4
    rotator.name = "R"
    
    n.addChild(rotator)
    
    //Back Button
    let back = SKTextButton(text: "Back", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.7) / 2), height: h / 10), functionToCall: {
        () -> () in
        let sc = (n.parent as! GameScene)
        sc.playChangingAnimation(sc.buildMenu)
    })
    back.position = CGPoint(x: mx - (back.size.width / 2), y: h - (h * 19)/20)
    back.changeColor(beige)
    back.label.fontSize = 40
    
    n.addChild(back)
    
    return n
}

func settingsTutPage() -> SKNode {
    let mx = screenSize.width / 2
    let h = screenSize.height
    
    let n = SKNode()
    
    //Items
    
    let p2 = wwPara("The dimensions of the game can be changed in the settings. ")
    let p3 = wwPara("This also changes the amount necessary for connect four.")
    let p4 = wwPara("The number at the bottom right indicates the amount needed to win.")
    let p5 = wwPara("The button placement setting represents how pieces can be placed.")
    let p6 = wwPara("In the “Use Buttons” setting, pieces are placed using the grey column indictors.")
    let p7 = wwPara("In the “Use Columns” setting, pieces can be placed by touching a column on the board.")
    let p8 = wwPara("The amount of players can also be changed.")
    
    let rotator = SKRotator(width: ((mx * 3.7) / 2), startIndex: 0, elements: p2, p3, p4, p5, p6, p7, p8)
    rotator.position.x = mx - ((mx * 3.7) / 4)
    rotator.position.y = h - (h * 8)/10
    rotator.aniSpeed = 0.4
    rotator.name = "R"
    
    n.addChild(rotator)
    
    //Back Button
    let back = SKTextButton(text: "Back", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.7) / 2), height: h / 10), functionToCall: {
        () -> () in
        let prev = mainTutPage()
        let rot = (prev.childNodeWithName("R") as! SKRotator)
        rot.aniSpeed = 0
        rot.setPosTo(2)
        prev.position.x = -screenSize.width
        prev.runAction(moveRight)
        n.parent?.addChild(prev)
        n.runAction(moveRight, completion: {
            n.removeFromParent()
        })
    })
    back.position = CGPoint(x: mx - (back.size.width / 2), y: h - (h * 19)/20)
    back.changeColor(beige)
    back.label.fontSize = 40
    
    n.addChild(back)
    
    return n
}

func stratsTutPage() -> SKNode {
    let mx = screenSize.width / 2
    let h = screenSize.height
    let w = screenSize.width
    
    let n = SKNode()
    
    //Items
    
    let p1 = wwPara("Typically the early game involving establishing board control.")
    p1.fontSize = 40
    p1.charWidth = Int(w * 2.4) / 40
    p1.spacing = 35
    
    let p2 = wwPara("This can be done by placing many pieces on the board together to prevent the enemy from flipping the board to ruin traps you set.")
    
    let p3 = wwPara("Inversely, your goal during the early game is also to stop your enemy from establishing board control as well.")
    
    let p4 = wwPara("An optimal early game would involve managing to split the enemy’s pieces into two sections and making a large bunch of pieces between them.")
    
    let p5 = wwPara("Careful, these are a winning positions:")
    
    let p6 = wwPara("Turn it upside down and...")
    
    let p7 = wwPara("Also,")
    
    let p8 = wwPara("Turn it right and...")
    
    //Examples
    
    let e1 = ex("competeForSpace")
    let e2 = ex("advEXbef1")
    let e3 = ex("advEXbef2")
    let e4 = ex("advEXaft1")
    let e5 = ex("advEXaft2")
    
    let rotator = SKRotator(width: ((mx * 3.7) / 2), startIndex: 0, elements: p1, p2, p3, p4, e1, p5, e2, p6, e4, p7, e3, p8, e5)
    rotator.position.x = mx - ((mx * 3.7) / 4)
    rotator.position.y = h - (h * 8)/10
    rotator.aniSpeed = 0.4
    rotator.name = "R"
    
    n.addChild(rotator)
    
    //Back Button
    let back = SKTextButton(text: "Back", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.7) / 2), height: h / 10), functionToCall: {
        () -> () in
        let prev = mainTutPage()
        let rot = (prev.childNodeWithName("R") as! SKRotator)
        rot.aniSpeed = 0
        rot.setPosTo(2)
        prev.position.x = -screenSize.width
        prev.runAction(moveRight)
        n.parent?.addChild(prev)
        n.position.x = w/10
        n.runAction(moveRight, completion: {
            n.removeFromParent()
        })
    })
    back.position = CGPoint(x: mx - (back.size.width / 2), y: h - (h * 19)/20)
    back.changeColor(beige)
    back.label.fontSize = 40
    
    n.addChild(back)
    
    return n
}

func refreshTutPage() -> SKNode {
    let mx = screenSize.width / 2
    let h = screenSize.height
    let w = screenSize.width
    
    let n = SKNode()
    
    //Items
    
    let p1 = wwPara("Players take turns dropping pieces into a 7x7 board with the goal of making a line of four pieces.")
    p1.fontSize = 40
    p1.charWidth = Int(w * 2.4) / 40
    p1.spacing = 35
    
    let p2 = wwPara("The pieces can form lines vertically, horizontally, and diagonally.")
    
    let p3 = wwPara("Here are some winning positions.")
    
    //Examples
    
    let e1 = ex("horzEX")
    let e2 = ex("vertEX")
    let e3 = ex("diagEX")
    
    let rotator = SKRotator(width: ((mx * 3.7) / 2), startIndex: 0, elements: p1, p2, p3, e1, e2, e3)
    rotator.position.x = mx - ((mx * 3.7) / 4)
    rotator.position.y = h - (h * 8)/10
    rotator.aniSpeed = 0.4
    rotator.name = "R"
    
    n.addChild(rotator)
    
    //Back Button
    let back = SKTextButton(text: "Back", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.7) / 2), height: h / 10), functionToCall: {
        () -> () in
        let prev = mainTutPage()
        let rot = (prev.childNodeWithName("R") as! SKRotator)
        rot.aniSpeed = 0
        rot.setPosTo(2)
        prev.position.x = -screenSize.width
        prev.runAction(moveRight)
        n.parent?.addChild(prev)
        n.runAction(moveRight, completion: {
            n.removeFromParent()
        })
    })
    back.position = CGPoint(x: mx - (back.size.width / 2), y: h - (h * 19)/20)
    back.changeColor(beige)
    back.label.fontSize = 40
    
    n.addChild(back)
    
    return n
}