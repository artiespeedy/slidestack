//
//  SKInfoBox.swift
//  SlideStack
//
//  Created by Alex Mai on 2/8/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKInfoBox: SKSpriteNode {
    var rectHeight: CGFloat = 60
    var rectSize: CGSize
    var rects: [SKSpriteNode]
    var strings: [String]
    var colors: [UIColor]
    var accessories: [[SKSpriteNode]]?
    
    init(strings: [String], colors: [UIColor], accessories: [[SKSpriteNode]]?, width: CGFloat) {
        self.strings = strings
        self.colors = colors
        self.accessories = accessories
        self.rects = []
        rectSize = CGSize(width: width, height: rectHeight)
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSize(width: width, height: CGFloat(strings.count) * rectHeight))
        for i in 0..<strings.count {
            let position = CGPoint(x: 0, y: CGFloat(i + 1) * rectHeight)
            let rect = SKSpriteNode(texture: nil, color: colors[i], size: size)
            rect.anchorPoint = CGPointZero
            rect.position = position
            if !strings[i].isEmpty {
                let textObject = SKLabelNode(text: strings[i])
                textObject.fontName = "Helvetica"
                textObject.fontColor = UIColor.blackColor()
                textObject.position = CGPoint(x: rect.size.width / 2, y: rectSize.height / 2)
                textObject.fontSize = 20
                rect.addChild(textObject)
            }
            
            rect.yScale = 0
            
            rects.append(rect)
            
            self.addChild(rects[rects.count - 1])
        }
        let dpc = UIColor(red: 0.9, green: 0.5, blue: 0.4, alpha: 1)
        let duc = UIColor(red: 0.9, green: 0.2, blue: 0.4, alpha: 1)
        
        let dismiss = SKRectButton(pressedColor: dpc, unpressedColor: duc, functionToCall: {
            () -> () in
            self.runAction(SKAction.scaleYTo(0, duration: 0.5))
        }, size: rectSize)
        dismiss.anchorPoint = CGPointZero
        dismiss.position = CGPointZero
        
        let textObject = SKLabelNode(text: "Dismiss")
        textObject.fontName = "Helvetica"
        textObject.fontColor = UIColor.blackColor()
        textObject.position = CGPoint(x: rectSize.width / 2, y: rectSize.height / 2)
        textObject.fontSize = 20
        dismiss.addChild(textObject)
        
        self.addChild(dismiss)
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.yScale = 0
        dismiss.position = CGPointZero
    }
    
    func expand() {
        for i in rects {
            i.runAction(SKAction.scaleYTo(1, duration: 0.5))
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}