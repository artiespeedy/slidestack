//
//  BoardClicker.swift
//  FlipSide
//
//  Created by Alex Mai on 3/10/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class BoardClicker: SKSpriteNode {
    let width: Int
    let height: Int
    var delegate: GameScene
    var points: [CGPoint] = []
    
    init(width: Int, height: Int, size: CGSize, delegate: GameScene) {
        self.width = width
        self.height = height
        self.delegate = delegate
        super.init(texture: nil, color: UIColor.clearColor(), size: size)
        anchorPoint = CGPointZero
        userInteractionEnabled = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        points.append(touches.first!.locationInNode(self))
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        points.append(touches.first!.locationInNode(self))
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        points.append(touches.first!.locationInNode(self))
        
        let gesture = recogGesture(points)
        if gesture != nil {
            switch(gesture!) {
            case Directs.Up:
                delegate.rotateUp()
            case Directs.Right:
                delegate.rotateRight()
            case Directs.Left:
                delegate.rotateLeft()
            default:
                ()
            }
        } else {
            let point = touches.first!.locationInNode(self)
            let rect = CGRect(origin: CGPointZero, size: self.size)
            print(point)
            print(rect)
            if rect.contains(point) {
                let column = Int(floor(Double(point.x) / Double(size.width / CGFloat(width))))
                delegate.board.place(column)
                delegate.updateBoard()
            }
        }
        
        points = []
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        points = []
    }
}