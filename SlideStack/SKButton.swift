//
//  SKButton.swift
//
//
//  Created by Alex Mai on 8/2/14.
//  Copyright (c) 2014 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKButton: SKSpriteNode {
    
    let pressedTexture : SKTexture
    let unpressedTexture : SKTexture
    var functionToCall : () -> ()
    var doesLoop = true
    var isCircle: Bool = false
    let bugFix: SKSpriteNode
    
    /*func setSize(newSize: CGSize) {
        self.size = newSize
        bugFix.size = newSize
    }*/
    
    var isPressed : Bool
    
    init(pressedTexture : SKTexture, unpressedTexture : SKTexture, functionToCall : () -> ())  {
        self.pressedTexture = pressedTexture
        self.unpressedTexture = unpressedTexture
        self.functionToCall = functionToCall
        isPressed = false
        bugFix = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: unpressedTexture.size())
        bugFix.anchorPoint = CGPointZero
        bugFix.position = CGPointZero
        super.init(texture: unpressedTexture, color: UIColor.clearColor(), size: unpressedTexture.size())
        anchorPoint = CGPointZero
        userInteractionEnabled = true
        //addChild(bugFix)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoder not supported in SKButton")
    }
    
    func press() {
        isPressed = true
        functionToCall()
        self.texture = pressedTexture
    }
    
    func unpress() {
        isPressed = false
        self.texture = unpressedTexture
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isCircle {
            var p = position
            if anchorPoint != CGPointZero {
                p = CGPoint(x: p.x - p.x * anchorPoint.x, y: p.y - p.y * anchorPoint.y)
            }
            let center = CGPoint(x: size.width / 2, y: size.height / 2)
            let radius = size.width / 2
            let point = touches.first!.locationInNode(self)
            let distance = sqrt(pow(Double(center.y - point.y), 2) + pow(Double(center.x - point.x), 2))
            if distance < Double(radius) {
                press()
            }
        } else {
            let point = touches.first!.locationInNode(self)
            let f = CGRect(origin: CGPointZero, size: size)
            if f.contains(point) {
                press()
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        var pressedThisEvent = false
        for i in touches {
            let location = i.locationInNode(self.parent!)
            if frame.contains(location) {
                if doesLoop {
                    press()
                }
                pressedThisEvent = true
            }
        }
        if !pressedThisEvent {
            unpress()
        }
        if !doesLoop {
            unpress()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        unpress()
    }
    
    func update() {
        if doesLoop {
            if isPressed {
                functionToCall()
            }
        }
    }
}

class SKRectButton: SKSpriteNode {
    
    let pressedColor : UIColor
    let unpressedColor : UIColor
    var functionToCall : () -> ()
    var doesLoop = false
    var isCircle: Bool = false
    let bugFix: SKSpriteNode
    
    var isPressed : Bool
    
    init(pressedColor: UIColor, unpressedColor: UIColor, functionToCall : () -> (), size: CGSize)  {
        self.pressedColor = pressedColor
        self.unpressedColor = unpressedColor
        self.functionToCall = functionToCall
        isPressed = false
        bugFix = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: size)
        bugFix.anchorPoint = CGPointZero
        bugFix.position = CGPointZero
        super.init(texture: nil, color: unpressedColor, size: size)
        anchorPoint = CGPointZero
        userInteractionEnabled = true
        //addChild(bugFix)
    }
    
    init(pressedColor: UIColor, unpressedColor: UIColor, functionToCall : () -> (), size: CGSize, texture: SKTexture)  {
        self.pressedColor = pressedColor
        self.unpressedColor = unpressedColor
        self.functionToCall = functionToCall
        isPressed = false
        
        bugFix = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: size)
        bugFix.anchorPoint = CGPointZero
        bugFix.position = CGPointZero
        
        super.init(texture: texture, color: unpressedColor, size: size)
        anchorPoint = CGPointZero
        userInteractionEnabled = true
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoder not supported in SKButton")
    }
    
    func press() {
        isPressed = true
        functionToCall()
        self.runAction(SKAction.colorizeWithColor(pressedColor, colorBlendFactor: 1, duration: 0))
    }
    
    func unpress() {
        isPressed = false
        self.runAction(SKAction.colorizeWithColor(unpressedColor, colorBlendFactor: 1, duration: 0))
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isCircle {
            let center = CGPoint(x: size.width / 2, y: size.height / 2)
            let radius = abs(center.y - position.y)
            let point = touches.first!.locationInNode(self)
            let distance = sqrt(pow(Double(center.y - point.y), 2) + pow(Double(center.x - point.x), 2))
            if distance < Double(radius) {
                press()
            }
        } else {
            let point = touches.first!.locationInNode(self)
            let f = CGRect(origin: CGPointZero, size: size)
            if f.contains(point) {
                press()
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        var pressedThisEvent = false
        for i in touches {
            let location = i.locationInNode(self.parent!)
            if frame.contains(location) {
                if doesLoop {
                    press()
                }
                pressedThisEvent = true
            }
        }
        if !pressedThisEvent {
            unpress()
        }
        if !doesLoop {
            unpress()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        unpress()
    }
    
    func update() {
        if doesLoop {
            if isPressed {
                functionToCall()
            }
        }
    }
}

class SKTextButton: SKSpriteNode {
    
    let pressedColor : UIColor
    let unpressedColor : UIColor
    var functionToCall : () -> ()
    var doesLoop = false
    var isCircle: Bool = false
    let bugFix: SKSpriteNode
    
    var isPressed : Bool
    
    var label: SKLabelNode
    
    var textAlignment: Directs = .None {
        didSet {
            switch(textAlignment) {
            case .Up:
                label.position = CGPoint(x: self.size.width / 2, y: self.size.height)
                label.verticalAlignmentMode = .Top
                label.horizontalAlignmentMode = .Center
            case .Right:
                label.position = CGPoint(x: self.size.width, y: self.size.height / 2)
                label.verticalAlignmentMode = .Center
                label.horizontalAlignmentMode = .Left
            case .Left:
                label.position = CGPoint(x: 0, y: self.size.height / 2)
                label.verticalAlignmentMode = .Center
                label.horizontalAlignmentMode = .Right
            case .Down:
                label.position = CGPoint(x: self.size.width / 2, y: 0)
                label.verticalAlignmentMode = .Bottom
                label.horizontalAlignmentMode = .Center
            case .None:
                label.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
                label.verticalAlignmentMode = .Center
                label.horizontalAlignmentMode = .Center
            }
        }
    }
    
    init(text: String, font: String?, alignment: Directs, pressedColor: UIColor, unpressedColor: UIColor, size: CGSize,functionToCall: () -> ())  {
        self.pressedColor = pressedColor
        self.unpressedColor = unpressedColor
        self.functionToCall = functionToCall
        
        label = SKLabelNode(text: text)
        label.fontSize = size.height * 0.8
        if font != nil {
            label.fontName = font
        }
        label.fontColor = pressedColor
        
        textAlignment = alignment
        
        isPressed = false
        
        bugFix = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: size)
        bugFix.anchorPoint = CGPointZero
        bugFix.position = CGPointZero
        
        super.init(texture: nil, color: unpressedColor, size: size)
        
        switch(textAlignment) {
        case .Up:
            label.position = CGPoint(x: self.size.width / 2, y: self.size.height)
            label.verticalAlignmentMode = .Top
            label.horizontalAlignmentMode = .Center
        case .Right:
            label.position = CGPoint(x: self.size.width, y: self.size.height / 2)
            label.verticalAlignmentMode = .Center
            label.horizontalAlignmentMode = .Left
        case .Left:
            label.position = CGPoint(x: 0, y: self.size.height / 2)
            label.verticalAlignmentMode = .Center
            label.horizontalAlignmentMode = .Right
        case .Down:
            label.position = CGPoint(x: self.size.width / 2, y: 0)
            label.verticalAlignmentMode = .Bottom
            label.horizontalAlignmentMode = .Center
        case .None:
            label.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
            label.verticalAlignmentMode = .Center
            label.horizontalAlignmentMode = .Center
        }
        
        self.addChild(label)
        anchorPoint = CGPointZero
        userInteractionEnabled = true
        //addChild(bugFix)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("NSCoder not supported in SKButton")
    }
    
    func press() {
        isPressed = true
        functionToCall()
        self.runAction(SKAction.colorizeWithColor(pressedColor, colorBlendFactor: 1, duration: 0))
        label.fontColor = unpressedColor
    }
    
    func unpress() {
        isPressed = false
        self.runAction(SKAction.colorizeWithColor(unpressedColor, colorBlendFactor: 1, duration: 0))
        label.fontColor = pressedColor
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isCircle {
            var p = position
            if anchorPoint != CGPointZero {
                p = CGPoint(x: p.x - p.x * anchorPoint.x, y: p.y - p.y * anchorPoint.y)
            }
            let center = CGPoint(x: size.width / 2, y: size.height / 2)
            let radius = abs(center.y - p.y)
            let distance = sqrt(pow(Double(center.y - p.y), 2) + pow(Double(center.x - p.x), 2))
            if distance < Double(radius) {
                press()
            }
        } else {
            let point = touches.first!.locationInNode(self)
            let f = CGRect(origin: CGPointZero, size: size)
            if f.contains(point) {
                press()
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        var pressedThisEvent = false
        for i in touches {
            let location = i.locationInNode(self.parent!)
            if frame.contains(location) {
                if doesLoop {
                    press()
                }
                pressedThisEvent = true
            }
        }
        if !pressedThisEvent {
            unpress()
        }
        if !doesLoop {
            unpress()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        unpress()
    }
    
    func update() {
        if doesLoop {
            if isPressed {
                functionToCall()
            }
        }
    }
}