//
//  SKRotator.swift
//  FlipSide
//
//  Created by Alex Mai on 3/12/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKRotator: SKNode {
    var elements: [SKNode]
    var moveRight: SKAction
    var moveLeft: SKAction
    var width: CGFloat
    var currentIndex: Int
    var aniSpeed: NSTimeInterval = 1 {
        didSet {
            moveRight = SKAction.moveByX(width + 10, y: 0, duration: aniSpeed)
            moveLeft = SKAction.moveByX(-width - 10, y: 0, duration: aniSpeed)
        }
    }
    
    private var locked = false
    private var next: SKTextButton
    private var prev: SKTextButton
    
    init(width: CGFloat, startIndex: Int = 0, elements e: SKNode...) {
        self.width = width
        self.elements = e
        currentIndex = startIndex
        moveRight = SKAction.moveByX(width, y: 0, duration: aniSpeed)
        moveLeft = SKAction.moveByX(-width, y: 0, duration: aniSpeed)
        
        next = SKTextButton(text: "Next", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: width / 2 - 5, height: screenSize.height / 10), functionToCall: {})
        next.name = "Next"
        next.position = CGPoint(x: next.size.width + 10, y: 0)
        
        prev = SKTextButton(text: "Prev", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: width / 2 - 5, height: screenSize.height / 10), functionToCall: {})
        prev.name = "Prev"
        prev.position = CGPointZero
        
        super.init()
        
        for i in elements {
            i.position.x = width * 2
        }
        elements[startIndex].position.x = 0
        addChild(elements[startIndex])
        
        next.functionToCall = {
            () -> () in
            print("Is locked: \(self.locked), index: \(self.currentIndex)")
            if !self.locked {
                self.locked = true
                if (self.currentIndex < self.elements.count - 1) {
                    self.elements[self.currentIndex].position.x = 0
                    //Start element to the left and move it in
                    self.elements[self.currentIndex + 1].position.x = width + 10
                    self.addChild(self.elements[self.currentIndex + 1])
                    self.elements[self.currentIndex].runAction(SKAction.moveByX(-width - 10, y: 0, duration: self.aniSpeed), completion: {
                        () -> () in
                        self.elements[self.currentIndex - 1].removeFromParent()
                    })
                    self.elements[self.currentIndex + 1].runAction(self.moveLeft)
                    self.currentIndex++
                }
                
                if self.prev.parent == nil {
                    self.addChild(self.prev)
                    self.prev.runAction(SKAction.fadeOutWithDuration(0))
                    self.prev.runAction(SKAction.fadeInWithDuration(0.5))
                }
                
                if ((self.elements.count - 1) == self.currentIndex) {
                    self.next.runAction(SKAction.fadeOutWithDuration(0.5), completion: {
                        () -> () in
                        self.childNodeWithName("Next")?.removeFromParent()
                    })
                }
                delay(self.aniSpeed, closure: {
                    () -> () in
                    self.locked = false
                })
            }
        }
        
        prev.functionToCall = {
            () -> () in
            print("Is locked: \(self.locked), index: \(self.currentIndex)")
            if !self.locked {
                self.locked = true
                if (self.currentIndex != 0) {
                    self.elements[self.currentIndex].position.x = 0
                    //Start element to the left and move it in
                    self.elements[self.currentIndex - 1].position.x = -width - 10
                    self.addChild(self.elements[self.currentIndex - 1])
                    self.elements[self.currentIndex].runAction(SKAction.moveByX(width + 10, y: 0, duration: self.aniSpeed), completion: {
                        () -> () in
                        self.elements[self.currentIndex + 1].removeFromParent()
                    })
                    self.elements[self.currentIndex - 1].runAction(self.moveRight)
                    self.currentIndex--
                }
                if self.next.parent == nil {
                    self.addChild(self.next)
                    self.next.runAction(SKAction.fadeOutWithDuration(0))
                    self.next.runAction(SKAction.fadeInWithDuration(0.5))
                }
                
                if self.currentIndex == 0 {
                    self.prev.runAction(SKAction.fadeOutWithDuration(0.5), completion: {
                        () -> () in
                        self.childNodeWithName("Prev")?.removeFromParent()
                    })
                }
                delay(self.aniSpeed, closure: {
                    () -> () in
                    self.locked = false
                })
            }
        }
        
        next.zPosition = 3
        prev.zPosition = 3
        
        addChild(next)
        addChild(prev)
        
        if ((self.elements.count - 1) == self.currentIndex) {
            self.childNodeWithName("Next")?.removeFromParent()
        }
        
        if self.currentIndex == 0 {
            self.childNodeWithName("Prev")?.removeFromParent()
        }
    }
    
    func setPosTo(index: Int) {
        if (index >= 0 && index < elements.count) && index != currentIndex {
            self.elements[self.currentIndex].removeFromParent()
            //All elements start to the right, now move them in
            self.addChild(self.elements[index])
            self.elements[index].position.x = -width - 10
            self.elements[index].runAction(self.moveRight)
            self.currentIndex = index
        }
        
        if next.parent == nil {
            self.addChild(next)
            self.next.runAction(SKAction.fadeOutWithDuration(0))
            self.next.runAction(SKAction.fadeInWithDuration(0.5))
        }
        
        if prev.parent == nil {
            self.addChild(prev)
            self.prev.runAction(SKAction.fadeOutWithDuration(0))
            self.prev.runAction(SKAction.fadeInWithDuration(0.5))
        }
        
        if ((self.elements.count - 1) == self.currentIndex) {
            self.childNodeWithName("Next")?.removeFromParent()
        }
        
        if self.currentIndex == 0 {
            self.childNodeWithName("Prev")?.removeFromParent()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}