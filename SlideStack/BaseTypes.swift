//
//  BaseTypes.swift
//  FlipSide
//
//  Created by Alex Mai on 3/11/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

enum Directs: Int {
    case Up = 0
    case Down = 1
    case Left = 2
    case Right = 3
    case None = 4
    
    func toString() -> String {
        switch (self.rawValue) {
        case 0:
            return "Up"
        case 1:
            return "Down"
        case 2:
            return "Left"
        case 3:
            return "Right"
        case 4:
            return "None"
        default:
            return "Shrooms"
        }
    }
}

extension SKSpriteNode {
    convenience init(imageWithoutAA : String) {
        let texture = SKTexture(imageNamed: imageWithoutAA)
        texture.filteringMode = SKTextureFilteringMode.Nearest
        self.init(texture: texture)
    }
}

extension SKNode {
    func changeColor(newColor: UIColor) {
        self.runAction(SKAction.colorizeWithColor(newColor, colorBlendFactor: 1, duration: 0))
    }
}

extension SKTexture {
    convenience init(textureWithoutAA : String) {
        self.init(imageNamed: textureWithoutAA)
        self.filteringMode = SKTextureFilteringMode.Nearest
    }
}

let color4value = [
    0:UIColor(red: 0, green: 0, blue: 0, alpha: 0),
    1:UIColor(red: 0.9, green: 0.2, blue: 0.3, alpha: 1),
    2:UIColor(red: 0.1, green: 0.3, blue: 0.9, alpha: 1),
    3:UIColor(red: 0.8375, green: 0.798, blue: 0.447, alpha: 1),
    4:UIColor(red: 0.2383, green: 0.7188, blue: 0.1445, alpha: 1)
]



