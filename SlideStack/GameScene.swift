//
//  GameScene.swift
//  SlideStack
//
//  Created by Alex Mai on 2/7/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import SpriteKit
import Darwin

let π = M_PI
let cgπ = CGFloat(M_PI)
let beige = UIColor(red: 0.88, green: 0.89, blue: 0.86, alpha: 1)
let black = UIColor(red: 0, green: 0, blue: 0, alpha: 1)

class GameScene: SKScene {
    
    var touchChain: [CGPoint] = []
    var numToConnect: Int = 4
    
    
    var board = SKBoard(width: 7, height: 7, amountOfPlayers: 2, size: CGSize(width: (screenSize.width * 18) / 20, height: (screenSize.width * 18) / 20))
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        buildMenu({})
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            touchChain.append(location)
        }
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        touchChain = []
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            touchChain.append(location)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            touchChain.append(location)
        }
        
        let gesture = recogGesture(touchChain)
        
        if gesture != nil {
            switch gesture! {
            case .Right:
                rotateRight()
            case .Left:
                rotateLeft()
            case .Up:
                rotateUp()
            default:
                ()
            }
        }
        
        touchChain = []
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func backToMenu() -> SKButton {
        let button = SKButton(pressedTexture: SKTexture(imageNamed: "pback.png"), unpressedTexture: SKTexture(imageNamed: "back.png"), functionToCall: {
            () -> () in
            self.playChangingAnimation(self.buildScene)
        })
        
        return button
    }
    
    func playChangingAnimation(middleFunction: (()->())->()) {
        let rightCurtain = coloredRect(CGRect(
            origin: CGPoint(x: 1, y: 0),
            size: CGSize(width: screenSize.width / 2 + 1, height: screenSize.height)), color: UIColor.blackColor())
        rightCurtain.anchorPoint = CGPoint(x: 1, y: 0)
        let leftCurtain = coloredRect(CGRect(
            origin: CGPoint(x: screenSize.width - 1, y: 0),
            size: CGSize(width: screenSize.width / 2 + 1, height: screenSize.height)), color: UIColor.blackColor())
        
        leftCurtain.name = "LeftCurtain"
        leftCurtain.zPosition = 20
        rightCurtain.name = "RightCurtain"
        rightCurtain.zPosition = 20
        
        let rightIn = SKAction.moveBy(CGVector(dx: screenSize.width / 2, dy: 0), duration: 0.2)
        let leftIn = SKAction.moveBy(CGVector(dx: -screenSize.width / 2, dy: 0), duration: 0.2)
        
        self.addChild(rightCurtain)
        self.addChild(leftCurtain)
        rightCurtain.runAction(rightIn)
        leftCurtain.runAction(leftIn, completion: {
            () -> () in
            self.removeAllChildren()
            
            self.clrBoard()
            self.addChild(rightCurtain)
            self.addChild(leftCurtain)
            let f: ()->() = {
                ()->() in
                middleFunction({
                    () -> () in
                    rightCurtain.runAction(leftIn)
                    leftCurtain.runAction(rightIn, completion: {
                        () -> () in
                        self.childNodeWithName("RightCurtain")?.removeFromParent()
                        self.childNodeWithName("LeftCurtain")?.removeFromParent()
                    })
                })
            }
            
            delay(0.1, closure: f)
        })
    }
    
    func clrBoard() {
        self.removeAllChildren()
        board.removeFromParent()
    }
    
    func buildMenu(callback: ()->()) {
        let mx = screenSize.width / 2
        let h = screenSize.height
        let gameTitle = SKLabelNode(text: "FlipSide")
        gameTitle.fontName = "HelveticaNeue-UltraLight"
        gameTitle.fontSize = 80
        gameTitle.position = CGPoint(x: mx, y: h - h/5)
        gameTitle.horizontalAlignmentMode = .Center
        
        let sm = ((mx * 3.5) / 4)
        
        let playGame = SKTextButton(text: "Start Game", font: nil, alignment: .None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: sm * 2, height: h / 10), functionToCall: {
            () -> () in
            self.playChangingAnimation(self.buildScene)
            })
        playGame.label.fontSize = 40
        playGame.position = CGPoint(x: mx - playGame.size.width / 2, y: h - (h * 3)/5)
        
        let options = SKTextButton(text: "Options", font: nil, alignment: .None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: playGame.size, functionToCall: {
            () -> () in
            self.playChangingAnimation(self.buildSettingsMenu)
            })
        options.label.fontSize = 40
        options.position = CGPoint(x: playGame.position.x, y: playGame.position.y - playGame.size.height - 10)
        
        let tutorial = SKTextButton(text: "How to Play", font: nil, alignment: .None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: playGame.size, functionToCall: {
            () -> () in
            self.playChangingAnimation(self.buildTutorialMenu)
        })
        tutorial.label.fontSize = 40
        tutorial.position = CGPoint(x: playGame.position.x, y: options.position.y - options.size.height - 10)
        
        self.addChild(tutorial)
        self.addChild(gameTitle)
        self.addChild(playGame)
        self.addChild(options)
        
        callback()
    }
    
    func buildTutorialMenu(callback: ()->()) {
        self.addChild(mainTutPage())
        callback()
    }
    
    func buildSettingsMenu(callback: ()->()) {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.valueForKey("Players") == nil {
            defaults.setInteger(7, forKey: "Dimensions")
            defaults.setInteger(2, forKey: "Players")
            defaults.setBool(true, forKey: "CanClickRow")
        }
        
        let mx = screenSize.width / 2
        let h = screenSize.height
        let pNumHead = SKLabelNode(text: "Number of Players")
        pNumHead.fontName = "HelveticaNeue-UltraLight"
        pNumHead.fontSize = 35
        pNumHead.position = CGPoint(x: mx, y: h - h/6)
        pNumHead.horizontalAlignmentMode = .Center
        
        let fPNum = SKNumField(range: 2..<5, startingValue: Float(defaults.valueForKey("Players")! as! NSNumber), incrementValue: 1, characterLength: 2, fontSize: 35, backgroundColor: beige)
        fPNum.textAlignment = .Center
        fPNum.position = CGPoint(x: mx, y: pNumHead.position.y - fPNum.size.height)
        
        let dimN = SKLabelNode(text: "Columns and Rows")
        dimN.fontName = "HelveticaNeue-UltraLight"
        dimN.fontSize = 35
        dimN.position = CGPoint(x: mx, y: h - (h * 7)/20)
        dimN.horizontalAlignmentMode = .Center
        
        let fDim = SKNumField(range: 0..<15, startingValue: Float(defaults.valueForKey("Dimensions")! as! NSNumber), incrementValue: 1, characterLength: 2, fontSize: 35, backgroundColor: beige)
        fDim.textAlignment = .Center
        fDim.position = CGPoint(x: mx, y: dimN.position.y - fDim.size.height)
        
        var canClickRow = Bool(defaults.valueForKey("CanClickRow")! as! NSNumber)
        
        let canClick = SKLabelNode(text: "Placing pieces")
        canClick.fontName = "HelveticaNeue-UltraLight"
        canClick.fontSize = 35
        canClick.position = CGPoint(x: mx, y: h - (h * 11)/20)
        canClick.horizontalAlignmentMode = .Center
        
        var lab: String
        
        if canClickRow {
            lab = "Use columns"
        } else {
            lab = "Use buttons"
        }
        
        let toggleCanClick = SKTextButton(text: lab, font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: (mx * 1.3), height: h / 10), functionToCall: {})
        toggleCanClick.functionToCall = {
            ()->() in
            canClickRow = !canClickRow
            if canClickRow {
                lab = "Use columns"
            } else {
                lab = "Use buttons"
            }
            toggleCanClick.label.text = lab
        }
        toggleCanClick.position = CGPoint(x: mx - (toggleCanClick.size.width / 2), y: canClick.position.y - toggleCanClick.size.height - (h * 1) / 20)
        toggleCanClick.label.fontSize = 35
        
        let back = SKTextButton(text: "Back", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.7) / 2), height: h / 10), functionToCall: {
            () -> () in
            defaults.setInteger(Int(fDim.fieldValue), forKey: "Dimensions")
            defaults.setInteger(Int(fPNum.fieldValue), forKey: "Players")
            defaults.setBool(canClickRow, forKey: "CanClickRow")
            
            self.playChangingAnimation(self.buildMenu)
        })
        back.position = CGPoint(x: mx - (back.size.width / 2), y: h - (h * 19)/20)
        back.runAction(SKAction.colorizeWithColor(beige, colorBlendFactor: 1, duration: 0))
        back.label.fontSize = 40
        
        self.addChild(dimN)
        self.addChild(pNumHead)
        self.addChild(fDim)
        self.addChild(fPNum)
        self.addChild(back)
        self.addChild(toggleCanClick)
        self.addChild(canClick)
        callback()
    }
    
    func buildScene(callback: ()->()) {
        
        let mx = screenSize.width / 2
        let h = screenSize.height
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.valueForKey("Players") == nil {
            defaults.setInteger(7, forKey: "Dimensions")
            defaults.setInteger(2, forKey: "Players")
            defaults.setBool(true, forKey: "CanClickRow")
        }
        numToConnect = Int(ceil(Double(defaults.valueForKey("Dimensions")! as! NSNumber) / 2))
        
        let constraints = SKNode()
        constraints.position = CGPoint(x: screenSize.width / 20, y: screenSize.height / 20)
        constraints.name = "Constraints"
        
        if h - (board.size.height + (h * 9) / 20) < 0 {
            board = SKBoard(width: Int(defaults.valueForKey("Dimensions")! as! NSNumber), height: Int(defaults.valueForKey("Dimensions")! as! NSNumber), amountOfPlayers: Int(defaults.valueForKey("Players")! as! NSNumber), size: CGSize(width: (screenSize.width * 15) / 20, height: (screenSize.width * 15) / 20))
        } else {
            board = SKBoard(width: Int(defaults.valueForKey("Dimensions")! as! NSNumber), height: Int(defaults.valueForKey("Dimensions")! as! NSNumber), amountOfPlayers: Int(defaults.valueForKey("Players")! as! NSNumber), size: CGSize(width: (screenSize.width * 18) / 20, height: (screenSize.width * 18) / 20))
        }
        
        board.position = CGPoint(x: mx - constraints.position.x, y: h - board.size.height - constraints.position.y - 10)
        board.name = "Board"
        board.changeColor(beige)
        board.position = CGPoint(
            x: board.position.x,
            y: board.position.y + board.size.height / 2)
        board.zPosition = 2
        constraints.addChild(board)
        
        
        
        let buttonHeight = board.position.y - 60 - board.size.height / 2
        for i in 0...board.width - 1 {
            let buttonFunc = {
                ()->() in
                self.board.place(i)
                self.updateBoard()
            }
            let grad = CGFloat(i) / CGFloat (board.width + 2)
            let pColor = UIColor(red: grad, green: grad, blue: grad, alpha: 1)
            let uColor = UIColor(red: grad + 0.1, green: grad + 0.1, blue: grad + 0.1, alpha: 1)
            let button = SKRectButton(pressedColor: pColor, unpressedColor: uColor, functionToCall: buttonFunc, size: CGSize(width: board.size.width / CGFloat(board.width), height: 60))
            button.position = CGPoint(x: (board.size.width / CGFloat(board.width)) * CGFloat(i) + (board.position.x - board.size.width / 2), y: buttonHeight)
            constraints.addChild(button)
        }
        
        let indictor = coloredRect(CGRect(
            x: mx - constraints.position.x,
            y: buttonHeight - (board.size.width / CGFloat(board.width)),
            width: board.size.width / CGFloat(board.width),
            height: board.size.width / CGFloat(board.width)),
            color: color4value[board.turnIndictor]!)
        indictor.anchorPoint = CGPoint(x: 0.5, y: 0)
        indictor.name = "Indict"
        constraints.addChild(indictor)
        
        let back = SKTextButton(text: "Back", font: nil, alignment: Directs.None, pressedColor: UIColor.blackColor(), unpressedColor: beige, size: CGSize(width: ((mx * 3.7) / 2), height: h / 10), functionToCall: {
            () -> () in
            self.playChangingAnimation(self.buildMenu)
        })
        back.position = CGPoint(x: mx - (back.size.width / 2), y: h - (h * 19)/20)
        back.runAction(SKAction.colorizeWithColor(beige, colorBlendFactor: 1, duration: 0))
        back.label.fontSize = 40
        self.addChild(back)
        
        let cirDia = buttonHeight - (back.size.height + back.position.y - constraints.position.y)
        
        let r = SKTexture(imageNamed: "r.png")
        let rp = SKTexture(imageNamed: "rp.png")
        
        let connectIndicator = SKLabelNode(text: "\(numToConnect)")
        connectIndicator.fontSize = 50
        connectIndicator.fontName = "HelveticaNeue-UltraLight"
        let backBHeight = (back.size.height + back.position.y - constraints.position.y)
        connectIndicator.position = CGPoint(x: ((screenSize.width * 3) / 4) - constraints.position.x, y: (backBHeight + abs(buttonHeight - (back.size.height + back.position.y - constraints.position.y)) / 2))
        connectIndicator.horizontalAlignmentMode = .Center
        connectIndicator.verticalAlignmentMode = .Center
        constraints.addChild(connectIndicator)
        
        let reload = SKButton(pressedTexture: r, unpressedTexture: rp, functionToCall: {
            () -> () in
            self.board.clear()
            self.board.runAction(SKAction.sequence([SKAction.colorizeWithColor(UIColor.blackColor(), colorBlendFactor: 1, duration: 0.4), SKAction.colorizeWithColor(beige, colorBlendFactor: 1, duration: 0.4)]))
        })
        reload.anchorPoint = CGPointZero
        reload.size = CGSize(width: cirDia - 20, height: cirDia - 20)
        reload.bugFix.size = CGSize(width: cirDia - 20, height: cirDia - 20)
        reload.position.y = connectIndicator.position.y - reload.size.height / 2
        reload.position.x = (connectIndicator.position.x - (reload.size.width / 2)) - mx
        reload.isCircle = true
        reload.changeColor(beige)
        constraints.addChild(reload)
        
        let connectCir = SKSpriteNode(imageNamed: "Circle.png")
        connectCir.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        connectCir.position = connectIndicator.position
        connectCir.size = reload.size
        connectCir.changeColor(black)
        
        constraints.addChild(connectCir)
        
        self.addChild(constraints)
        
        if Bool(defaults.valueForKey("CanClickRow") as! NSNumber) {
            let clicker = BoardClicker(width: board.width, height: board.height, size: board.size, delegate: self)
            clicker.position = CGPoint(x: board.position.x - board.size.width / 2, y: board.position.y - board.size.height / 2)
            clicker.zPosition = 18
            clicker.name = "BoardClicker"
            constraints.addChild(clicker)
        }
        
        callback()
    }
    
    func updateBoard() {
        let winner = board.scanForConnect(numToConnect)
        if winner != nil {
            for i in winner! {
                if i.1 {
                    end(i.0)
                    break
                }
            }
        }
        self.childNodeWithName("Constraints")?.childNodeWithName("Indict")?.runAction(SKAction.colorizeWithColor(color4value[board.turnIndictor]!, colorBlendFactor: 1, duration: 0))
    }
    
    func rotateRight() {
        self.childNodeWithName("Constraints")?.childNodeWithName("Indict")?.runAction(SKAction.colorizeWithColor(color4value[board.turnIndictor]!, colorBlendFactor: 1, duration: 0))
        let scaleUp = SKAction.scaleBy(1.1, duration: 0.2)
        let rot = SKAction.rotateByAngle(cgπ / 2, duration: 0.7)
        let scaleDown = SKAction.scaleTo(1, duration: 0.2)
        board.skip()
        let onComp = {()->() in
			switch self.board.gravDirect {
			case .Up:
				self.board.shift(.Right)
			case .Down:
				self.board.shift(.Left)
			case .Right:
				self.board.shift(.Down)
			case .Left:
				self.board.shift(.Up)
			default:
				self.board.shift(.Up)
			}
            self.updateBoard()
        }
        let runBoard = SKAction.runBlock(onComp)
        let full = SKAction.sequence([scaleUp, rot, scaleDown, runBoard])
        self.childNodeWithName("Constraints")?.childNodeWithName("Board")?.runAction(full)
    }
    
    func rotateLeft() {
        self.childNodeWithName("Constraints")?.childNodeWithName("Indict")?.runAction(SKAction.colorizeWithColor(color4value[board.turnIndictor]!, colorBlendFactor: 1, duration: 0))
        let scaleUp = SKAction.scaleBy(1.1, duration: 0.2)
        let rot = SKAction.rotateByAngle(cgπ / -2, duration: 0.7)
        let scaleDown = SKAction.scaleTo(1, duration: 0.2)
        let onComp = {()->() in
            self.board.skip()
            switch self.board.gravDirect {
            case .Up:
                self.board.shift(.Left)
            case .Down:
                self.board.shift(.Right)
            case .Right:
                self.board.shift(.Up)
            case .Left:
                self.board.shift(.Down)
            default:
                self.board.shift(.Up)
            }
            self.updateBoard()
        }
        let runBoard = SKAction.runBlock(onComp)
        let full = SKAction.sequence([scaleUp, rot, scaleDown, runBoard])
        self.childNodeWithName("Constraints")?.childNodeWithName("Board")?.runAction(full)
    }
    
    func rotateUp() {
        self.childNodeWithName("Constraints")?.childNodeWithName("Indict")?.runAction(SKAction.colorizeWithColor(color4value[board.turnIndictor]!, colorBlendFactor: 1, duration: 0))
        let scaleUp = SKAction.scaleBy(1.1, duration: 0.2)
        let rot = SKAction.rotateByAngle(cgπ, duration: 0.7)
        let scaleDown = SKAction.scaleTo(1, duration: 0.2)
        board.skip()
        let onComp = {()->() in
			switch self.board.gravDirect {
			case .Up:
				self.board.shift(.Down)
			case .Down:
				self.board.shift(.Up)
			case .Right:
				self.board.shift(.Left)
			case .Left:
				self.board.shift(.Right)
			default:
				self.board.shift(.Up)
			}
            self.updateBoard()
        }
        let runBoard = SKAction.runBlock(onComp)
        let full = SKAction.sequence([scaleUp, rot, scaleDown, runBoard])
        self.childNodeWithName("Constraints")?.childNodeWithName("Board")?.runAction(full)
    }
    
    func coloredRect(frame: CGRect, color: UIColor) -> SKSpriteNode {
        let rect = SKSpriteNode(texture: nil, color: color, size: frame.size)
        rect.position = frame.origin
        rect.anchorPoint = CGPointZero
        return rect
    }
    
    func end(winner: Int) {
        self.childNodeWithName("Constraints")?.childNodeWithName("BoardClicker")?.removeFromParent()
        if winner != 0 {
            let messageLayer = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: CGSize(width: screenSize.width * 2, height: screenSize.height * 1.5))
            messageLayer.position = CGPointZero
            messageLayer.zPosition = 10
            messageLayer.name = "MSL"
            messageLayer.userInteractionEnabled = true
            self.addChild(messageLayer)
            let constraints = self.childNodeWithName("Constraints")!
            let beige = UIColor(red: 0.88, green: 0.89, blue: 0.78, alpha: 1)
            let info = SKInfoAlert(message: "\(Piece.colorForValue(winner)) won!", messageColor: beige, boxSize: CGSize(width: screenSize.width * 4 / 5, height: screenSize.height / 10), onDismiss: {
                () -> () in
                self.childNodeWithName("MSL")?.removeFromParent()
                let defaults = NSUserDefaults.standardUserDefaults()
                if Bool(defaults.valueForKey("CanClickRow") as! NSNumber) {
                    let clicker = BoardClicker(width: self.board.width, height: self.board.height, size: self.board.size, delegate: self)
                    clicker.position = CGPoint(x: self.board.position.x - self.board.size.width / 2, y: self.board.position.y - self.board.size.height / 2)
                    clicker.zPosition = 18
                    clicker.name = "BoardClicker"
                    constraints.addChild(clicker)
                }
            })
            //info.anchorPoint = CGPointZero
            info.position = CGPoint(x: screenSize.width / 2 - info.size.width / 2, y: screenSize.height / 2 - info.size.height / 2)
            messageLayer.addChild(info)
            info.expand()
        }
    }
}
