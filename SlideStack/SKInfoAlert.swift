//
//  SKInfoAlert.swift
//  SlideStack
//
//  Created by Alex Mai on 2/8/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKInfoAlert: SKSpriteNode {
    var message: String
    var messageColor: UIColor
    var boxSize: CGSize
    
    var messageRect: SKSpriteNode
    var dismissRect: SKSpriteNode
    
    init(message: String, messageColor: UIColor, boxSize: CGSize) {
        self.message = message
        self.messageColor = messageColor
        self.boxSize = boxSize
        
        self.messageRect = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: CGSizeZero)
        self.dismissRect = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: CGSizeZero)
        
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSize(width: boxSize.width, height: boxSize.height * 2))
        
        messageRect = SKSpriteNode(texture: nil, color: messageColor, size: boxSize)
        let textObject = SKLabelNode(text: message)
        textObject.fontName = "HelveticaNeue-UltraLight"
        textObject.fontColor = UIColor.blackColor()
        textObject.position = CGPoint(x: boxSize.width / 2, y: messageRect.frame.height / 2)
        textObject.horizontalAlignmentMode = .Center
        textObject.verticalAlignmentMode = .Center
        textObject.fontSize = 35
        messageRect.addChild(textObject)
        messageRect.anchorPoint = CGPoint(x: 0, y: 0)
        messageRect.position = CGPoint(x: 0, y: boxSize.height)
        
        let dpc = UIColor(red: 0.9, green: 0.5, blue: 0.4, alpha: 1)
        let duc = UIColor(red: 0.9, green: 0.2, blue: 0.4, alpha: 1)
        
        dismissRect = SKRectButton(pressedColor: dpc, unpressedColor: duc, functionToCall: {
            self.collapse()
        }, size: boxSize)
        dismissRect.zPosition = 100
        let dismissText = SKLabelNode(text: "Dismiss")
        dismissText.fontName = "HelveticaNeue-UltraLight"
        dismissText.fontColor = UIColor.blackColor()
        dismissText.fontSize = 35
        dismissText.position = CGPoint(x: boxSize.width / 2, y: dismissText.frame.height)
        dismissText.horizontalAlignmentMode = .Center
        dismissText.verticalAlignmentMode = .Center
        dismissRect.addChild(dismissText)
        dismissRect.anchorPoint = CGPoint(x: 0, y: 0)
        dismissRect.position = CGPoint(x: 0, y: 0)
        dismissText.zPosition = 100
        
        self.addChild(dismissRect)
        self.addChild(messageRect)
        
        collapse()
        
    }
    init(message: String, messageColor: UIColor, boxSize: CGSize, onDismiss: ()->()) {
        self.message = message
        self.messageColor = messageColor
        self.boxSize = boxSize
        
        self.messageRect = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: CGSizeZero)
        self.dismissRect = SKSpriteNode(texture: nil, color: UIColor.clearColor(), size: CGSizeZero)
        
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSize(width: boxSize.width, height: boxSize.height * 2))
        
        messageRect = SKSpriteNode(texture: nil, color: messageColor, size: boxSize)
        let textObject = SKLabelNode(text: message)
        textObject.fontName = "HelveticaNeue-UltraLight"
        textObject.fontColor = UIColor.blackColor()
        textObject.position = CGPoint(x: boxSize.width / 2, y: messageRect.frame.height / 2)
        textObject.horizontalAlignmentMode = .Center
        textObject.verticalAlignmentMode = .Center
        textObject.fontSize = 35
        messageRect.addChild(textObject)
        messageRect.anchorPoint = CGPoint(x: 0, y: 0)
        messageRect.position = CGPoint(x: 0, y: boxSize.height)
        
        let dpc = UIColor(red: 0.9, green: 0.5, blue: 0.2, alpha: 1)
        let duc = UIColor(red: 0.9, green: 0.2, blue: 0.2, alpha: 1)
        
        dismissRect = SKRectButton(pressedColor: dpc, unpressedColor: duc, functionToCall: {
            ()->() in
            self.collapseAndCall(onDismiss)
            }, size: boxSize)
        let dismissText = SKLabelNode(text: "Dismiss")
        dismissText.fontName = "HelveticaNeue-UltraLight"
        dismissText.fontColor = UIColor.blackColor()
        dismissText.position = CGPoint(x: boxSize.width / 2, y: dismissText.frame.height)
        dismissText.horizontalAlignmentMode = .Center
        dismissText.verticalAlignmentMode = .Center
        dismissText.fontSize = 35
        dismissRect.addChild(dismissText)
        dismissRect.anchorPoint = CGPoint(x: 0, y: 0)
        dismissRect.position = CGPoint(x: 0, y: 0)
        
        self.addChild(dismissRect)
        self.addChild(messageRect)
        
        collapse()
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collapse() {
        messageRect.runAction(SKAction.scaleYTo(0, duration: 0.2))
        dismissRect.runAction(SKAction.group([SKAction.scaleYTo(0, duration: 0.2), SKAction.moveToY(boxSize.height, duration: 0.2)]))
    }
    
    func collapseAndCall(call: ()->()) {
        messageRect.runAction(SKAction.scaleYTo(0, duration: 0.2), completion: call)
        dismissRect.runAction(SKAction.group([SKAction.scaleYTo(0, duration: 0.2), SKAction.moveToY(boxSize.height, duration: 0.2)]))
    }
    
    func expand() {
        messageRect.runAction(SKAction.scaleYTo(1, duration: 0.2))
        dismissRect.runAction(SKAction.group([SKAction.scaleYTo(1, duration: 0.2), SKAction.moveToY(0, duration: 0.2)]))
    }
    
}