//
//  Piece.swift
//  FlipSide
//
//  Created by Alex Mai on 3/12/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class Piece: SKSpriteNode {
    var value: Int {
        didSet {
            let change = SKAction.colorizeWithColor(color4value[value]!, colorBlendFactor: 1, duration: 0)
            self.runAction(change)
        }
    }
    
    init(value: Int, size: CGSize) {
        self.value = value
        super.init(texture: SKTexture(imageNamed: "Circle.png"), color: color4value[value]!, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var colorName: String {
        switch value {
        case 0:
            return "Clear"
        case 1:
            return "Red"
        case 2:
            return "Blue"
        case 3:
            return "Yellow"
        default:
            return "Clear"
        }
    }
    
    class func colorForValue(value: Int) -> String {
        switch value {
        case 1:
            return "Red"
        case 2:
            return "Blue"
        case 3:
            return "Yellow"
        case 4:
            return "Green"
        default:
            return "Clear"
        }
    }
}

func == (lhs: Piece, rhs: Piece) -> Bool {
    return lhs.value == rhs.value
}

func == (lhs: Piece, rhs: Int) -> Bool {
    return lhs.value == rhs
}


func != (lhs: Piece, rhs: Piece) -> Bool {
    return lhs.value != rhs.value
}

func != (lhs: Piece, rhs: Int) -> Bool {
    return lhs.value != rhs
}