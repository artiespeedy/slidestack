//
//  Board.swift
//  SlideStack
//
//  Created by Alex Mai on 2/7/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

class Board {
    var data: [[Int]]
    //goes from 1 to amtofplayers
    var turnIndictor: Int
    let amtOfPlayers: Int
    var gravDirect = Directs.Up
    
    init(width: Int, height: Int, amountOfPlayers: Int) {
        data = [[Int]](count: width, repeatedValue: [Int](count: height, repeatedValue: 0))
        amtOfPlayers = amountOfPlayers
        turnIndictor = 1
    }
    
    var width: Int {
        get {
            return data.count
        }
    }
    
    var height: Int {
        get {
            return data[0].count
        }
    }
    
    func skip() {
        if (turnIndictor == amtOfPlayers) {
            turnIndictor = 1
        } else {
            turnIndictor++
        }
    }
    
    var isDone: Bool {
        get {
            for x in data {
                for i in x {
                    if (i == 0) {return false}
                }
            }
            return true
        }
    }
    
    //Returns how far down it went
    func place(index: Int) -> Int {
        //Starts at the bottom and goes up to scan for empty slots
        switch gravDirect {
        case .Up:
            // If the top row isn't full
            if data[index][data[index].count - 1] == 0 {
                var distanceDown = data[0].count + 1
                for i in 0..<data[index].count {
                    distanceDown--
                    if data[index][i] == 0 {
                        data[index][i] = turnIndictor
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        case .Down:
            // If the bottom row isn't full
            if data[index][0] == 0 {
                var distanceDown = data[0].count + 1
                for y in 0..<data[index].count {
                    distanceDown--
                    let i = data[index].count - y - 1
                    if data[index][i] == 0 {
                        data[index][i] = turnIndictor
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        case .Right:
            if data[0][index] == 0 {
                //Y is fixed to the selected row, go through each value for x and look at the fixed y height
                var distanceDown = data.count + 1
                for y in 0..<data.count {
                    distanceDown--
                    let i = data.count - y - 1
                    //If it is free, turn it and stop
                    if data[i][index] == 0 {
                        data[i][index] = turnIndictor
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        case .Left:
            if data[data.count - 1][index] == 0 {
                //Y is fixed to the selected row, go through each value for x and look at the fixed y height
                var distanceDown = data.count + 1
                for i in 0..<data.count {
                    distanceDown--
                    //If it is free, turn it and stop
                    if data[i][index] == 0 {
                        data[i][index] = turnIndictor
                        break
                    }
                }
                skip()
                return distanceDown
            } else {
                return 0
            }
        default:
            return 0
        }
    }
    
    //Returns (x, y, amount moved in new direction)
    func shift(direction: Directs) -> [(Int, Int, Int)] {
        //Store lowest piece per row
        self.gravDirect = direction
        
        var piecesMoved: [(Int, Int, Int)] = []
        
        //Start from the bottom and start shift items down
        switch gravDirect {
        case .Up:
            for x in 0..<data.count {
                var furthestEmptySlot = 0
                for y in 0..<data[0].count {
                    //If the current slot has a piece
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == y) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot++
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            let distance = y - furthestEmptySlot
                            piecesMoved.append((x, y, distance))
                            data[x][furthestEmptySlot] = data[x][y]
                            data[x][y] = 0
                            furthestEmptySlot++
                        }
                    }
                }
            }
        case .Down:
            for x in 0..<data.count {
                var furthestEmptySlot = data[0].count - 1
                //Count backwards
                for p in 0..<data[0].count {
                    let y = data[0].count - p - 1
                    //If the current slot has a piece
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == y) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot--
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            let distance = furthestEmptySlot - y
                            piecesMoved.append((x, y, distance))
                            data[x][furthestEmptySlot] = data[x][y]
                            data[x][y] = 0
                            furthestEmptySlot--
                        }
                    }
                }
            }
        case .Right:
            for y in 0..<data[0].count {
                var furthestEmptySlot = data.count - 1
                for p in 0..<data.count {
                    let x = data.count - p - 1
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == x) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot--
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            let distance = x - furthestEmptySlot
                            piecesMoved.append((x, y, distance))
                            data[furthestEmptySlot][y] = data[x][y]
                            data[x][y] = 0
                            furthestEmptySlot--
                        }
                    }
                }
            }
        case .Left:
            for y in 0..<data[0].count {
                var furthestEmptySlot = 0
                for x in 0..<data.count {
                    if (data[x][y] != 0) {
                        //And the current slot is the same slot as the furthest empty slot
                        if (furthestEmptySlot == x) {
                            //Shift it up. Clearly its not empty.
                            furthestEmptySlot++
                        } else {
                            //Its assumed that the furthestEmptySlot is below
                            //Shift the piece down and the empty slot up
                            let distance = furthestEmptySlot - x
                            piecesMoved.append((x, y, distance))
                            data[furthestEmptySlot][y] = data[x][y]
                            data[x][y] = 0
                            furthestEmptySlot++
                        }
                    }
                }
            }
        default:
            NSException(name: "NAD", reason: "A non-acceptable direction was sent to Board.swift", userInfo: nil).raise()
        }
        
        return piecesMoved
    }
    
    //Int player who won
    func scanForConnect(length: Int) -> [Int: Bool]? {
        var winners : [Int: Bool] = [:]
        var didEnd = false
        for i in 1..<amtOfPlayers {
            
            //Add the player to the list
            winners[i] = false
            
            //Scan for straight up
            for x in 0..<data.count {
                var currentChain = 0
                
                for y in 0..<data[0].count {
                    if data[x][y] == i {
                        currentChain++
                    } else {
                        currentChain = 0
                    }
                }
                
                if currentChain >= length {
                    winners[i] = true
                    didEnd = true
                }
            }
            
            //Scan for horizontal
            for y in 0..<data[0].count {
                var currentChain = 0
                
                for x in 0..<data.count {
                    if data[x][y] == i {
                        currentChain++
                    } else {
                        currentChain = 0
                    }
                }
                
                if currentChain >= length {
                    winners[i] = true
                    didEnd = true
                }
            }
            
            //Scan for diagonal up left to down right
            for _ in 0..<data[0].count {
                var y = data[0].count - 1
                while ((y >= 0) && y < data.count) {
                    let x = y
                    var currentChain = 0
                    if data[x][y] == i {
                        currentChain++
                    } else {
                        currentChain = 0
                    }
                    y--
                    if currentChain >= length {
                        winners[i] = true
                        didEnd = true
                        break
                    }
                }
            }
            
            //Scan for diagonal down left to up right
            for _ in 0..<data[0].count {
                var y = 0
                while ((y > 0) && y >= 0) {
                    let x = y
                    var currentChain = 0
                    if data[x][y] == i {
                        currentChain++
                    } else {
                        currentChain = 0
                    }
                    y++
                    if currentChain >= length {
                        winners[i] = true
                        didEnd = true
                        break
                    }
                }
            }
            
        }
        //if there are winners, return winners, otherwise return nil
        if didEnd {
            return winners
        } else {
            return nil
        }
    }
    
    func clear() {
        for x in 0..<data.count {
            for y in 0..<data[0].count {
                data[x][y] = 0
            }
        }
    }
}