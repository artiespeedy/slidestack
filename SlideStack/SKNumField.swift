//
//  SKNumField.swift
//  FlipSide
//
//  Created by Alex Mai on 3/7/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class SKNumField: SKSpriteNode {
	var fieldValue: Float
	var incrementValue: Float
    var range: Range<Int>
    var isDecimal: Bool = false {
        didSet {
            if isDecimal {
                field.text = "\(fieldValue)"
            } else {
                field.text = "\(Int(fieldValue))"
            }
        }
    }

	let decButton: SKTextButton
	let incButton: SKTextButton
	let field: SKLabelNode
	let root: SKNode
    let backgroundColor: UIColor
    
    var textAlignment: SKLabelHorizontalAlignmentMode = .Left {
        didSet {
            field.horizontalAlignmentMode = textAlignment
            switch(textAlignment) {
            case .Center:
                field.position = CGPoint(x: self.size.width / 2, y: self.size.height / 10)
            case .Left:
                field.position = CGPoint(x: 0, y: self.size.height / 10)
            case .Right:
                field.position = CGPoint(x: self.size.width, y: self.size.height / 10)
            }
        }
    }

    init (range: Range<Int>,startingValue: Float, incrementValue: Float, characterLength: Int, fontSize: Int, backgroundColor: UIColor) {
        self.range = range
		self.fieldValue = startingValue
		self.incrementValue = incrementValue
        field = SKLabelNode(text: "\(fieldValue)")
        field.fontSize = CGFloat(fontSize)
        field.fontColor = UIColor.blackColor()
        field.fontName = "HelveticaNeue-UltraLight"
        self.backgroundColor = backgroundColor
        
        let bSize = CGSize(width: fontSize, height: fontSize)
        decButton = SKTextButton(text: "-", font: "HelveticaNeue-UltraLight", alignment: .None, pressedColor: UIColor.blackColor(), unpressedColor: UIColor.clearColor(), size: bSize, functionToCall: {})
        incButton = SKTextButton(text: "+", font: "HelveticaNeue-UltraLight", alignment: .None, pressedColor: UIColor.blackColor(), unpressedColor: UIColor.clearColor(), size: bSize, functionToCall: {})
        
        decButton.runAction(SKAction.colorizeWithColor(backgroundColor, colorBlendFactor: 1.0, duration: 0))
        incButton.runAction(SKAction.colorizeWithColor(backgroundColor, colorBlendFactor: 1.0, duration: 0))

        root = SKNode()
        root.addChild(decButton)
        root.addChild(incButton)
        root.addChild(field)

        super.init(texture: nil, color: backgroundColor, size: CGSize(width: CGFloat(characterLength * fontSize), height: CGFloat(fontSize)))
        
        decButton.anchorPoint = CGPointZero
        decButton.position = CGPoint(x: -decButton.size.width - 5, y: 0)
        field.horizontalAlignmentMode = textAlignment
        incButton.anchorPoint = CGPointZero
        incButton.position = CGPoint(x: CGFloat(characterLength * fontSize) + 5, y: 0)
        
        decButton.functionToCall = dec
        incButton.functionToCall = inc
        
        root.position = CGPoint(x: self.size.width / -2, y: self.size.height / -2)

        self.addChild(root)
        
        if isDecimal {
            field.text = "\(fieldValue)"
        } else {
            field.text = "\(Int(fieldValue))"
        }
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	func inc() {
        if (fieldValue + incrementValue) > Float(range.endIndex) {
            fieldValue = Float(range.endIndex)
        } else {
            fieldValue+=incrementValue
        }
        if isDecimal {
            field.text = "\(fieldValue)"
        } else {
            field.text = "\(Int(fieldValue))"
        }
        incButton.runAction(SKAction.colorizeWithColor(backgroundColor, colorBlendFactor: 1.0, duration: 0))
	}

	func dec() {
        if (fieldValue - incrementValue) < Float(range.startIndex) {
            fieldValue = Float(range.startIndex)
        } else {
            fieldValue-=incrementValue
        }
        if isDecimal {
            field.text = "\(fieldValue)"
        } else {
            field.text = "\(Int(fieldValue))"
        }
        decButton.runAction(SKAction.colorizeWithColor(backgroundColor, colorBlendFactor: 1.0, duration: 0))
    }
}
