//
//  GestureRecognizer.swift
//  SlideStack
//
//  Created by Alex Mai on 2/7/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x - rhs.x, y: lhs.y - rhs.y)
}

func recogGesture(points: [CGPoint]) -> Directs? {
    let riseOrun = points.last! - points[0]
    let length = sqrt(
        pow(Double(riseOrun.x), 2) + pow(Double(riseOrun.y), 2)
    )
    
    if length > 100 {
        if riseOrun.x > 100 {
            return .Left
        } else if riseOrun.x < -100 {
            return .Right
        } else if riseOrun.y > 100 {
            return .Up
        } else if riseOrun.y < -100 {
            return .Down
        } else {
            return nil
        }
    } else {
        return nil
    }
}